require "mp3info"

class RecordingsController < ApplicationController
  def index
    redirect_to :action => "list_owned"
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update, :import ],
         :redirect_to => { :action => :list }

  def list
    user_id = cookies[:user_id]
    if user_id.nil?
      logger.add(Logger::DEBUG, "No user_id cookie found")
      redirect_to :controller => "users", :action => "login"
      return
    end

    @owner = User.find(user_id)
    @recording_pages, @recordings = paginate :recordings, :per_page => 10,
	:order => "`when` desc"
    cookies[:recording_goal] = "list"
    cookies[:recording_page] = @recording_pages.current.to_i.to_s
  end

  def list_owned
    cookies[:recording_goal] = "list_owned"
    cookies.delete :recording_page
    user_id = cookies[:user_id]
    if user_id.nil?
      logger.add(Logger::DEBUG, "No user_id cookie found")
      redirect_to :controller => "users", :action => "login"
      return
    end

    @owner = User.find(user_id)
    if @owner.nil?
      logger.add(Logger::DEBUG, "No user found")
      redirect_to :controller => "users", :action => "login"
      return
    end
    @recording_pages, @recordings = paginate :recordings, 
	:per_page => 10,
	:conditions => [ "(user_id = ? OR user_id IS NULL) && archived = 0", 
			   user_id],
	:order => "`when` desc"
    cookies[:recording_page] = @recording_pages.current.to_i.to_s
  end

  def show
    @recording = Recording.find(params[:id])
    @back = cookies[:recording_goal]
    @back_page = cookies[:recording_page]
  end

  def edit
    @recording = Recording.find(params[:id])
    @back = cookies[:recording_goal]
    @back_page = cookies[:recording_page]
  end

  def update
    @recording = Recording.find(params[:id])
    @back = cookies[:recording_goal]
    @back_page = cookies[:recording_page]
    if @recording.update_attributes(params[:recording])
      flash[:notice] = 'Recording was successfully updated.'
      if @back.nil?
        redirect_to :action => 'show', :id => @recording
      else
        redirect_to :action => @back, :page => @back_page
      end
    else
      render :action => 'edit'
    end
  end

  def play
    send_data(url_for(:action => "mp3", 
		        :id => params[:id]),
	      :type => "audio/x-mpegurl",
	      :disposition => "inline")
  end

  def mp3
    send_file(Recording.find(params[:id]).file,
	      :disposition => "inline")
  end

  def import
    @recordings = []
    dir = VOICE_SPOOL
    Dir.entries(dir + "/incoming").each do |filename|
      if filename =~ /^v-([0-9]+)-([0-9]+)-([A-Z0-9]+)\.rmd$/
	pid = $1
 	timestamp = $2
	cid = $3
	mp3file = dir + "/mp3/v-" + pid + "-" + timestamp + "-" + cid + ".mp3"
	record_time = Time.at(timestamp.to_i)
	id3title = "Nr " + cid + record_time.strftime(" at %Y-%m-%d %H:%M:%S.")
	id3artist = cid
	id3album = record_time.strftime("Answering machine %Y-%m-%d")
        system("rmdtopvf " + dir + "/incoming/" + 
		filename + "| pvftowav " +
		"| lame -h --add-id3v2" + 
		" --tt '" + id3title + "'" + 
		" --ta '" + id3artist + "'" + 
		" --tl '" + id3album + "'" + 
		" - " + mp3file)
	mp3info = Mp3Info.new(mp3file)
	recording = Recording.new(:cid => cid,
		    		  :when => record_time,
		      		  :file => mp3file,
				  :length => mp3info.length,
		      		  :seen => 0,
		      		  :archived => 0)
	recording.save!
	@recordings << recording
	File.rename(dir + "/incoming/" + filename,
		    dir + "/archived/" + filename)
      end
    end
    @recordings.sort!
    @back = cookies[:recording_goal]
    @back_page = cookies[:recording_page]
  end

  def take
    recording = Recording.find(params[:id])
    user_id = cookies[:user_id]
    if user_id.nil?
      logger.add(Logger::DEBUG, "No user_id cookie found")
      redirect_to :controller => "users", :action => "login"
      return
    end

    owner = User.find(user_id)
    if owner.nil?
      logger.add(Logger::DEBUG, "User not found")
      redirect_to :controller => "users", :action => "logout"
      return
    end

    recording.user_id = owner.id
    recording.save!
    rec_id = recording["when"].strftime("%Y-%m-%d&nbsp;%H:%M:%S")
    flash[:notice] = 'Recording ' + rec_id + ' taken.'
    redirect_to :action => cookies[:recording_goal],
		:page => cookies[:recording_page]
  end

  def give
    recording = Recording.find(params[:id])
    user = User.find(params[:user_id])
    if user.nil?
      logger.add(Logger::DEBUG, "User to give to not found")
      return
    end

    recording.user_id = user.id
    recording.save!
    rec_id = recording["when"].strftime("%Y-%m-%d&nbsp;%H:%M:%S")
    flash[:notice] = 'Recording ' + rec_id + ' given to ' + user.username + '.'
    redirect_to :action => cookies[:recording_goal],
		:page => cookies[:recording_page]
  end

  def archive
    recording = Recording.find(params[:id])
    user_id = cookies[:user_id]
    if user_id.nil?
      logger.add(Logger::DEBUG, "No user_id cookie found")
      redirect_to :controller => "users", :action => "login"
      return
    end

    owner = User.find(user_id)
    if owner.nil?
      logger.add(Logger::DEBUG, "User not found")
      redirect_to :controller => "users", :action => "logout"
      return
    end

    recording.archived = 1
    recording.save!
    rec_id = recording["when"].strftime("%Y-%m-%d&nbsp;%H:%M:%S")
    flash[:notice] = 'Recording ' + rec_id + ' archived.'
    redirect_to :action => cookies[:recording_goal],
		:page => cookies[:recording_page]
  end
end
