class CallAttemptsController < ApplicationController
  def index
    list
    render :action => 'list'
  end

  # GETs should be safe (see http://www.w3.org/2001/tag/doc/whenToUseGet.html)
  verify :method => :post, :only => [ :destroy, :create, :update ],
         :redirect_to => { :action => :list }

  def list
    @call_attempt_pages, @call_attempts = paginate :call_attempts, :per_page => 10
  end

  def show
    @call_attempt = CallAttempt.find(params[:id])
  end

  def new
    @call_attempt = CallAttempt.new
  end

  def create
    @call_attempt = CallAttempt.new(params[:call_attempt])
    if @call_attempt.save
      flash[:notice] = 'CallAttempt was successfully created.'
      redirect_to :action => 'list'
    else
      render :action => 'new'
    end
  end

  def edit
    @call_attempt = CallAttempt.find(params[:id])
  end

  def update
    @call_attempt = CallAttempt.find(params[:id])
    if @call_attempt.update_attributes(params[:call_attempt])
      flash[:notice] = 'CallAttempt was successfully updated.'
      redirect_to :action => 'show', :id => @call_attempt
    else
      render :action => 'edit'
    end
  end

  def destroy
    CallAttempt.find(params[:id]).destroy
    redirect_to :action => 'list'
  end
end
