class Recording < ActiveRecord::Base
  belongs_to :user

  def <=>(other)
    # Reverse the order, to get the newest entry first.
    other["when"] <=> self["when"]
  end
end
