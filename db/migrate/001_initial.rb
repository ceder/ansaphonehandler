class Initial < ActiveRecord::Migration
  def self.up

    create_table (:users, 
		  :options => 'ENGINE=InnoDB DEFAULT CHARSET=utf8') do |t|
      t.column :username, :string
      t.column :created_on, :timestamp
      t.column :updated_on, :timestamp
    end

    create_table (:recordings, 
		  :options => 'ENGINE=InnoDB DEFAULT CHARSET=utf8') do |t|
      t.column :cid,      :string
      t.column :from,     :string
      t.column :comment,  :string
      t.column :when,     :datetime
      t.column :user_id,  :integer
      t.column :file,	  :string
      t.column :length,	  :string
      t.column :seen,     :integer
      t.column :archived, :integer
      t.column :created_on, :timestamp
      t.column :updated_on, :timestamp
    end

    create_table (:call_attempts, 
		  :options => 'ENGINE=InnoDB DEFAULT CHARSET=utf8') do |t|
      t.column :cid,      :string
      t.column :from,     :string
      t.column :when,     :datetime
      t.column :created_on, :timestamp
      t.column :updated_on, :timestamp
    end
  end

  def self.down
    drop_table :users
    drop_table :recordings
    drop_table :call_attempts
  end
end
