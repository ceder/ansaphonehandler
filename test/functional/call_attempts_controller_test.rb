require File.dirname(__FILE__) + '/../test_helper'
require 'call_attempts_controller'

# Re-raise errors caught by the controller.
class CallAttemptsController; def rescue_action(e) raise e end; end

class CallAttemptsControllerTest < Test::Unit::TestCase
  fixtures :call_attempts

  def setup
    @controller = CallAttemptsController.new
    @request    = ActionController::TestRequest.new
    @response   = ActionController::TestResponse.new
  end

  def test_index
    get :index
    assert_response :success
    assert_template 'list'
  end

  def test_list
    get :list

    assert_response :success
    assert_template 'list'

    assert_not_nil assigns(:call_attempts)
  end

  def test_show
    get :show, :id => 1

    assert_response :success
    assert_template 'show'

    assert_not_nil assigns(:call_attempt)
    assert assigns(:call_attempt).valid?
  end

  def test_new
    get :new

    assert_response :success
    assert_template 'new'

    assert_not_nil assigns(:call_attempt)
  end

  def test_create
    num_call_attempts = CallAttempt.count

    post :create, :call_attempt => {}

    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_equal num_call_attempts + 1, CallAttempt.count
  end

  def test_edit
    get :edit, :id => 1

    assert_response :success
    assert_template 'edit'

    assert_not_nil assigns(:call_attempt)
    assert assigns(:call_attempt).valid?
  end

  def test_update
    post :update, :id => 1
    assert_response :redirect
    assert_redirected_to :action => 'show', :id => 1
  end

  def test_destroy
    assert_not_nil CallAttempt.find(1)

    post :destroy, :id => 1
    assert_response :redirect
    assert_redirected_to :action => 'list'

    assert_raise(ActiveRecord::RecordNotFound) {
      CallAttempt.find(1)
    }
  end
end
